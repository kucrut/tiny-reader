# Tiny Reader

Tiny reader is a [Progressive Web Application](https://web.dev/progressive-web-apps/) for [Tiny Tiny RSS](https://tt-rss.org/) built with [SvelteKit](https://kit.svelte.dev).

<!-- List features & limitations here -->

## Demo

If you have access to a Tiny Tiny RSS installation with API access enabled for your user, feel free to use the serverless version at https://tiny-reader.netlify.app to see how it works.

## Setup

There are a few ways to run Tiny Reader.

### Docker Compose

1. Make sure [Docker Compose](https://docs.docker.com/compose/) is setup.
1. Setup [Traefik Proxy](https://gitlab.com/wp-id/docker/traefik-proxy).
1. Grab the latest compose files archive from the [releases page](https://gitlab.com/kucrut/tiny-reader/-/releases).
1. Extract the archive and enter the extracted directory, then follow the instructions in the README file.

### Docker

If you already have Tiny Tiny RSS running via Docker Compose and you know your way around setting up the compose file, add a service using the [`kucrut/tiny-reader`](https://hub.docker.com/r/kucrut/tiny-reader/tags) image.

### Serverless

If you already have Tiny Tiny RSS running somewhere publicly accessible, you can deploy Tiny Reader to one of the serverless providers that are supported by [SvelteKit's auto adapter](https://kit.svelte.dev/docs/adapter-auto) by forking/importing this repo to your git hosting account and then setting up the deployment. Make sure to set the build command to `pnpm build` and the publish or output directory to `build`.

### Manual Single Page Application setup (Nginx, Apache, etc)

If you already have Tiny Tiny RSS running, you can run Tiny Reader along side it. Grab the latest SPA package from the [releases page](https://gitlab.com/kucrut/tiny-reader/-/releases):

- get the _Single Page Application (root directory)_: package if you want to install it on you web server root directory (ie. TTRSS is installed in a sub directory),
- or get the _Single Page Application (sub directory /tr)_: package if you want to install it on you web server sub directory `/tr`.

**IMPORTANT**

- Make sure to enable API Access for your Tiny Tiny RSS user.
- If you have OTP enabled for your Tiny Tiny RSS user, make sure to create an App Password and use that password on Tiny Reader login screen.

## Developing

Tiny Reader requires Node 18+ and [pnpm](https://pnpm.io), so make sure they are installed.

Clone the repository and install the dependencies...

```sh
git clone https://gitlab.com/kucrut/tiny-reader
cd tiny-reader
pnpm install
pnpm dev
```

Navigate to [http://localhost:5173](http://http://localhost:5173). On the login screen, set TT-RSS URL to the full URL or your Tiny Tiny RSS installation (eg. https://mydomain.com/tt-rss).

If you don't have Tiny Tiny RSS running, you can run it locally Docker Compose from the [compose](./docker/compose) directory. Make sure to follow the instructions in the README file.

### Try production build locally

Assuming you're using the compose files from this repo to run Tiny Tiny RSS, here's how to try the production build locally:

```sh
pnpm build
cd docker/compose
docker compose down
# Edit .env and update the COMPOSE_FILE value:
# COMPOSE_FILE=docker-compose.yml:docker-compose.override.tiny-reader-local-build.yml
docker compose up
```

### Editor Setup

To make Prettier work on Visual Studio Code, install the extension and add `"**/*.svelte"` to `"prettier.documentSelectors"` in `settings.json`. ([ref](https://github.com/sveltejs/prettier-plugin-svelte/issues/155#issuecomment-831166730))
