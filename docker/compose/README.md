# Tiny Reader with Docker Compose

## Usage

1. Setup [Traefik Proxy](https://gitlab.com/wp-id/docker/traefik-proxy).
1. Copy `.env-dist` to `.env` and edit some values accordingly.
1. Run `docker compose up -d`.
