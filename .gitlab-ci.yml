---
# Overview
#
# 1. On merge requests:
#   - Run `check_format` job.
#   - If this is not a release MR:
#     - Run linters if there are changes in relevant files.
#     - Build app if there are changes in relevant files.
# 2. On merge to default branch (main):
#   - If theres a new `.md` file in `.changesets` directory, create MR for a new release.
#   - If the `CHANGELOG.md` file changed (meaning we have merged the above MR):
#     1. Build app, both for installation in root and sub directories.
#     2. Build docker image.
#     3. Publish docker image.
#     3. Publish packages.
#     4. Create release.

## Globals
stages:
  - lint
  - build
  - pre-publish
  - publish

variables:
  CHANGESET_RELEASE_BRANCH: changeset-release/main
  PNPM_HOME: .pnpm
  SUBDIR_BASE_ROUTE: tr

## Base configurations for jobs

.rule_changelog_changed: &rule_changelog_changed
  - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    changes:
      - "CHANGELOG.md"

.with_node:
  image: kucrut/pnpm:18
  interruptible: true
  cache:
    key:
      files:
        - pnpm-lock.yaml
    paths:
      - $PNPM_HOME

.with_docker:
  image: docker:latest
  interruptible: true
  before_script:
    - echo "$DOCKER_PASSWORD" | docker login docker.io --username $DOCKER_USER --password-stdin
  services:
    - docker:dind

.base_publish:
  stage: publish
  rules:
    - *rule_changelog_changed
    - if: $CI_COMMIT_TAG
      when: never

## Jobs

allow_merge_new_version:
  stage: lint
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - "CHANGELOG.md"
  script: echo true

check_format:
  extends: .with_node
  stage: lint
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME != 'changeset-release/main'
  script:
    - pnpm install --silent
    - pnpm format:check

lint_css:
  extends: .with_node
  stage: lint
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - "**/*.css"
        - "**/*.html"
        - "**/*.pcss"
        - "**/*.svelte"
        - "pnpm-lock.yaml"
  script:
    - pnpm install --silent
    - pnpm lint:css

lint_js:
  extends: .with_node
  stage: lint
  rules:
    - if: $CI_MERGE_REQUEST_ID
      changes:
        - "**/*.cjs"
        - "**/*.html"
        - "**/*.js"
        - "**/*.svelte"
        - "**/*.ts"
        - "pnpm-lock.yaml"
  script:
    - pnpm install --silent
    - pnpm lint:js

build_app_for_merge_request:
  extends: .with_node
  stage: build
  rules:
    - if: $CI_MERGE_REQUEST_ID && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME != $CHANGESET_RELEASE_BRANCH
      changes:
        - "**/*.cjs"
        - "**/*.css"
        - "**/*.html"
        - "**/*.js"
        - "**/*.pcss"
        - "**/*.svelte"
        - "pnpm-lock.yaml"
  script:
    - pnpm install --silent
    - ADAPTER=static pnpm build

create_new_version_merge_request:
  extends: .with_node
  stage: pre-publish
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - ".changeset/**"
      exists:
        - ".changeset/*.md"
    - if: $CI_COMMIT_TAG
      when: never
  script: |
    pnpm install --silent
    pnpx changesets-gitlab

build_app_for_main:
  extends: .with_node
  stage: build
  rules:
    - *rule_changelog_changed
  script:
    - |
      export VERSION=$(cat package.json | grep version | head -1 | awk -F: '{ print $2 }' | sed 's/[", ]//g')
      echo "VERSION=${VERSION}" > variables.env
      pnpm install --silent
      ADAPTER=node pnpm build
      ADAPTER=static pnpm build
      ADAPTER=static BASE_ROUTE="/${SUBDIR_BASE_ROUTE}" pnpm build
  artifacts:
    name: "${CI_PROJECT_NAME}-${VERSION}"
    paths:
      - build
    reports:
      dotenv: variables.env

publish_docker_images:
  extends: .with_docker
  stage: publish
  rules:
    - *rule_changelog_changed
  needs:
    - job: build_app_for_main
      artifacts: true
  script: |
    docker build -t $CI_PROJECT_PATH -f docker/Dockerfile .
    docker push $CI_PROJECT_PATH
    NEW_IMAGE_TAG="${CI_PROJECT_PATH}:${VERSION}"
    docker tag $CI_PROJECT_PATH $NEW_IMAGE_TAG
    docker push $NEW_IMAGE_TAG

publish_packages:
  extends: .base_publish
  image: curlimages/curl:latest
  needs:
    - job: build_app_for_main
      artifacts: true
    - job: publish_docker_images
  variables:
    ENV_FILE: "${CI_PROJECT_DIR}/packages.env"
    PACKAGE_URL_PREFIX: "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/Web/${VERSION}"
  script:
    - |
      PACKAGE_BASENAME="${CI_PROJECT_NAME}-spa-${VERSION}"
      PACKAGE_FILENAME="${PACKAGE_BASENAME}.tar.bz2"
      PACKAGE_FILEPATH="${CI_PROJECT_DIR}/${PACKAGE_FILENAME}"
      PACKAGE_URL="${PACKAGE_URL_PREFIX}/${PACKAGE_FILENAME}"
      cp -r build/spa $PACKAGE_BASENAME
      tar -cjf $PACKAGE_FILEPATH $PACKAGE_BASENAME
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_FILEPATH $PACKAGE_URL
      echo "SPA_PACKAGE_URL_ROOT=${PACKAGE_URL}" >> $ENV_FILE
    - |
      PACKAGE_BASENAME="${CI_PROJECT_NAME}-spa-subdir-${VERSION}"
      PACKAGE_FILENAME="${PACKAGE_BASENAME}.tar.bz2"
      PACKAGE_FILEPATH="${CI_PROJECT_DIR}/${PACKAGE_FILENAME}"
      PACKAGE_URL="${PACKAGE_URL_PREFIX}/${PACKAGE_FILENAME}"
      cp -r "build/spa-${SUBDIR_BASE_ROUTE}" $PACKAGE_BASENAME
      tar -cjf $PACKAGE_FILEPATH $PACKAGE_BASENAME
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_FILEPATH $PACKAGE_URL
      echo "SPA_PACKAGE_URL_SUBDIR=${PACKAGE_URL}" >> $ENV_FILE
    - |
      PACKAGE_BASENAME="${CI_PROJECT_NAME}-nodejs-${VERSION}"
      PACKAGE_FILENAME="${PACKAGE_BASENAME}.tar.bz2"
      PACKAGE_FILEPATH="${CI_PROJECT_DIR}/${PACKAGE_FILENAME}"
      PACKAGE_URL="${PACKAGE_URL_PREFIX}/${PACKAGE_FILENAME}"
      cp -r build/node $PACKAGE_BASENAME
      tar -cjf $PACKAGE_FILEPATH $PACKAGE_BASENAME
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_FILEPATH $PACKAGE_URL
      echo "NODEJS_PACKAGE_URL=${PACKAGE_URL}" >> $ENV_FILE
    - |
      PACKAGE_BASENAME="${CI_PROJECT_NAME}-docker-compose"
      PACKAGE_FILENAME="${PACKAGE_BASENAME}-${VERSION}.tar.bz2"
      PACKAGE_FILEPATH="${CI_PROJECT_DIR}/${PACKAGE_FILENAME}"
      PACKAGE_URL="${PACKAGE_URL_PREFIX}/${PACKAGE_FILENAME}"
      mkdir $PACKAGE_BASENAME
      cp docker/compose/.env-dist docker/compose/docker-compose.override.https.yml docker/compose/README.md $PACKAGE_BASENAME
      sed -E "s#(image: kucrut/tiny-reader)\$#\1:${VERSION}#" docker/compose/docker-compose.yml > "${PACKAGE_BASENAME}/docker-compose.yml"
      tar -cjf $PACKAGE_FILEPATH $PACKAGE_BASENAME
      curl --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file $PACKAGE_FILEPATH "${PACKAGE_URL}"
      echo "DOCKER_COMPOSE_PACKAGE_URL=${PACKAGE_URL}" >> $ENV_FILE
  artifacts:
    reports:
      dotenv: packages.env

create_release:
  extends: .base_publish
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  needs:
    - job: build_app_for_main
      artifacts: true
    - job: publish_docker_images
    - job: publish_packages
      artifacts: true
  script: echo "Creating release $VERSION..."
  release:
    name: "${CI_PROJECT_TITLE} $VERSION"
    description: 'Docker image: "${CI_PROJECT_PATH}:${VERSION}"'
    tag_name: $VERSION
    ref: $CI_COMMIT_SHA
    assets:
      links:
        - name: "Single Page Application (root directory)"
          url: $SPA_PACKAGE_URL_ROOT
        - name: "Single Page Application (sub directory /tr)"
          url: $SPA_PACKAGE_URL_SUBDIR
        - name: "Node JS"
          url: $NODEJS_PACKAGE_URL
        - name: "Docker Compose"
          url: $DOCKER_COMPOSE_PACKAGE_URL
