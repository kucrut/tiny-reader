/**
 * Convert YouTube channel URL to its feed URL
 *
 * @param {string} url YouTube channel URL.
 * @return {string} Converted URL if successful.
 */
export function convert_youtube_channel_url( url ) {
	const matched = url.match(
		/(?:(?:http|https):\/\/|)(?:www\.|)youtube\.com\/(?:channel|user)\/([A-Z][a-zA-Z0-9\-_]{1,})/,
	);

	if ( matched ) {
		return `https://www.youtube.com/feeds/videos.xml?channel_id=${ matched[ 1 ] }`;
	}

	return url;
}

/**
 * Extract YouTube video ID from URL
 *
 * @param {string} url URL.
 * @return {string|undefined} YouTube video ID or undefined.
 */
export function extract_youtube_video_id( url ) {
	const matches = url.match( /youtu(?:.*\/v\/|.*v=|\.be\/)([A-Za-z0-9_-]{11})/ );

	if ( matches ) {
		return matches[ 1 ];
	}

	return undefined;
}
