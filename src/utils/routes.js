import { base } from '$app/paths';

/**
 * Create route path with base prepended
 *
 * @param {string} path Path suffix.
 * @return {string} Route path.
 */
export function create_route_path( path ) {
	return `${ base }/${ path.replace( /^[/]+|[/]+$/g, '' ) }`;
}
