import { backOut } from 'svelte/easing';

export const fadeIn = { duration: 130 };
export const flyDown = { duration: 200, y: -50, opacity: 0.5, easing: backOut };
export const flyUp = { duration: 200, y: 50, opacity: 0.5, easing: backOut };
