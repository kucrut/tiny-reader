import dompurify from 'dompurify';

/**
 * Remove style attributes from element
 *
 * @param {Element} node Element.
 */
function remove_style_attributes( node ) {
	node.removeAttribute( 'bgcolor' );
	node.removeAttribute( 'border' );
	node.removeAttribute( 'cellpadding' );
	node.removeAttribute( 'cellspacing' );
	node.removeAttribute( 'valign' );
	node.removeAttribute( 'width' );

	return node;
}

/**
 * Sanitize link attributes
 *
 * @param {Element} node Anchor element.
 */
function sanitize_link( node ) {
	node.setAttribute( 'rel', 'noopener noreferrer' );
	node.setAttribute( 'target', '_blank' );

	return node;
}

/**
 * Sanitize image element
 *
 * @param {Element} node Image element.
 */
function sanitize_image( node ) {
	if ( ! node.hasAttribute( 'alt' ) ) {
		node.setAttribute( 'alt', '' );
	}

	if ( ! node.hasAttribute( 'loading' ) ) {
		node.setAttribute( 'loading', 'lazy' );
	}

	return node;
}

/**
 * Sanitize audio & video element
 *
 * @param {Element} node Image element.
 */
function sanitize_audio_video( node ) {
	node.removeAttribute( 'autoplay' );
	node.removeAttribute( 'loop' );
	node.removeAttribute( 'playsinline' );
	node.removeAttribute( 'preload' );

	return node;
}

/**
 * Sanitize table
 *
 * @param {Element} _node Table element.
 */
function sanitize_table( _node ) {
	const node = remove_style_attributes( _node );

	if ( node.parentNode?.nodeName !== 'BODY' ) {
		return node;
	}

	const wrapper = document.createElement( 'div' );
	wrapper.classList.add( 'tr-table-wrapper' );

	node.parentNode?.insertBefore( wrapper, node );
	wrapper.appendChild( node );

	return node;
}

export function add_dom_hooks() {
	dompurify.addHook( 'uponSanitizeElement', node => {
		switch ( node.nodeName ) {
			case 'AUDIO':
			case 'VIDEO':
				return sanitize_audio_video( node );
			case 'TABLE':
				return sanitize_table( node );
			case 'TD':
			case 'TH':
				return remove_style_attributes( node );
			default:
				return node;
		}
	} );

	dompurify.addHook( 'afterSanitizeAttributes', node => {
		switch ( node.nodeName ) {
			case 'A':
				return sanitize_link( node );
			case 'IMG':
				return sanitize_image( node );
			default:
				return node;
		}
	} );
}
