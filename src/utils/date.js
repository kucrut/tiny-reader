/**
 * Format date relatively
 *
 * Credit: https://writingjavascript.com/format-5-days-ago-localized-relative-date-strings-in-a-few-lines-with-native-javascript
 *
 * @param {Date|number}       value  Date timestamp or object.
 * @param {string|undefined=} locale Locale to use.
 * @return {string}            Relative date.
 */
export function format_days_ago( value, locale ) {
	const date = new Date( value );
	const deltaDays = ( date.getTime() - Date.now() ) / ( 1000 * 3600 * 24 );

	if ( deltaDays < -7 ) {
		return date.toLocaleDateString( undefined, {
			day: 'numeric',
			month: 'short',
			year: 'numeric',
		} );
	}

	const formatter = new Intl.RelativeTimeFormat( locale, { numeric: 'auto' } );

	return formatter.format( Math.round( deltaDays ), 'days' );
}
