import { init_stores } from '$stores';

export const csr = true;
export const ssr = false;

/** @type { import('./$types').LayoutLoad } */
export function load( { fetch } ) {
	const stores = init_stores( fetch );

	return { stores };
}
