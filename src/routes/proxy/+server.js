/** @type {import('./$types').RequestHandler} */
export async function POST( { fetch, request } ) {
	const { _endpoint, ...body } = await request.json();

	const response = await fetch( _endpoint, {
		body: JSON.stringify( body ),
		method: 'post',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json; charset=utf-8',
		},
	} );

	return response;
}
