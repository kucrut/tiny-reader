import type { Readable, Writable } from 'svelte/store';

// See https://stackoverflow.com/a/54827898
export type BetterOmit< T, K extends PropertyKey > = { [ P in keyof T as Exclude< P, K > ]: T[ P ] };

export type EngineName = 'tt-rss';

export interface SigninParams {
	engine: EngineName;
	password: string;
	url: string;
	username: string;
}

export interface Auth {
	engine: EngineName;
	id: string;
	url: string;
}

export type AuthStore = Readable< Auth >;

export interface EngineAuth {
	signin: ( params: SigninParams ) => Promise< Auth >;
	signout: ( params: Auth ) => Promise< boolean >;
	validate: ( params: Auth ) => Promise< boolean >;
}

export type Theme = 'system' | 'light' | 'dark';

export interface Settings {
	fetch_limit: number;
	fetch_older_first: boolean;
	fetch_only_unread: boolean;
	theme: Theme;
	use_low_profile: boolean;
}

export type SettingsStore = Writable< Settings >;

export interface Attachment {
	duration?: number;
	height?: number;
	mime_type: string;
	title: string;
	url: string;
	width?: number;
}

export interface Article {
	attachments?: Attachment[];
	author: string;
	content: string;
	datedisplay: string;
	datetime: string;
	excerpt: string;
	feed_id: number;
	id: number;
	link: string;
	starred: boolean;
	title: string;
	thumbnail: string;
	unread: boolean;
}

export type ArticleParams = Record< string, unknown >;

export interface Category {
	favicon?: string;
	href: string;
	icon: string;
	id: number;
	title: string;
	unread: number;
}

export interface Feed extends Category {
	is_category_feed: boolean;
	menu_title?: string;
}

export interface SourceTreeItem extends Category {
	feeds: Feed[];
}

export interface Session {
	is_checked: boolean;
	is_valid: boolean;
	redirect_to: string;
}

export interface EngineData< T > {
	error?: Error | null;
	has_items: boolean;
	has_more: boolean;
	is_fetched: boolean;
	is_fetching: boolean;
	is_fetching_more: boolean;
	items: T[];
	items_by_id: Map< number, T >;
}

export interface EngineDataStore< T > extends Readable< EngineData< T > > {
	fetch: () => Promise< void >;
	get_single: ( id: number ) => T | undefined;
}

export interface ArticlesStore extends EngineDataStore< Article > {
	fetch_more: () => Promise< void >;
	fetch_single: ( id: number ) => Promise< void >;
	refresh: () => Promise< void >;

	get_params: () => ArticleParams;
	set_params: ( params: ArticleParams ) => void;

	get_next: ( id: number ) => Article | undefined;
	get_previous: ( id: number ) => Article | undefined;

	toggle_read: ( ids: number[], is_read: boolean ) => Promise< void >;
	toggle_starred: ( ids: number[], is_starred: boolean ) => Promise< void >;
}

export type Articles = EngineData< Article >;
export type Categories = EngineData< Category >;
export type CategoriesStore = EngineDataStore< Category >;
export type Feeds = EngineData< Feed >;
export type FeedsStore = EngineDataStore< Feed >;
export type SourceTree = EngineData< SourceTreeItem >;
export type SourceTreeStore = EngineDataStore< SourceTreeItem >;

export type EngineDataTypes = {
	articles: ArticlesStore;
	categories: CategoriesStore;
	feeds: FeedsStore;
	source_tree: SourceTreeStore;
};

export interface FeedOption {
	title: string;
	url: string;
}

export interface SubscribeResult {
	feeds?: FeedOption[];
	message?: string;
	success?: boolean;
}

export interface Engine {
	get_data< K extends keyof EngineDataTypes >( type: K ): EngineDataTypes[ K ];
	init: () => Promise< void >;
	signin: ( params: SigninParams ) => Promise< Auth >;
	signout: ( params: Auth ) => Promise< boolean >;
	subscribe: (
		feed_url: string,
		category_id: number,
		params?: Record< string, unknown >,
	) => Promise< SubscribeResult >;
	unsubscribe: ( feed_id: number ) => Promise< boolean >;
	validate_session: ( params: Auth ) => Promise< boolean >;
}

export type EngineFunc = ( fetch_fn: typeof fetch, auth: AuthStore, settings: SettingsStore ) => Engine;

export interface FeedToSubscribeOption {
	checked: boolean;
	label: string;
	value: string;
}

export interface ActionButtonProps {
	class?: string;
	icon: string;
	label: string;
	onClick?: ( e: Event ) => void;
	[ k: string ]: unknown;
}
