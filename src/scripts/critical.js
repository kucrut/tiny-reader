( function () {
	let savedTheme;

	window.__setTheme = function ( theme ) {
		document.documentElement.className = theme === 'system' ? '' : 'theme-' + theme;
	};

	try {
		const settings = localStorage.getItem( 'tiny-reader-settings' );

		if ( ! settings ) {
			window.__setTheme( 'system' );
			return;
		}

		savedTheme = JSON.parse( settings ).theme || 'system';

		if ( savedTheme !== 'system' ) {
			window.__setTheme( savedTheme );
		}
	} catch {
		window.__setTheme( 'system' );
	}
} )();
