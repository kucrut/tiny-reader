import script from './scripts/critical.js?raw';
import styles from '$styles/critical.pcss?inline';
import svg_sprite from '$components/svg-sprite.svelte?raw';

/** @type {import('@sveltejs/kit').Handle} */
export function handle( { event, resolve } ) {
	return resolve( event, {
		transformPageChunk: ( { html } ) => {
			return html
				.replace( '%critical_style%', `<style>${ styles.trim() }</style>` )
				.replace( '%critical_script%', `<script>${ script.trim() }</script>` )
				.replace( '%svg_sprite%', `<div class="svg-sprite">${ svg_sprite }</div>` );
		},
	} );
}
