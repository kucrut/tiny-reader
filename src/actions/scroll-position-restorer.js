/** @typedef {Record<string, number>} Collection */
/** @typedef {{id: string; restore: boolean } | null} Param */

import { writable } from 'svelte/store';

/**
 * Scroll position restorer
 *
 * @param {HTMLElement} node Element to get/set scroll position from/to. Fallbacks to root element (html).
 * @param {Param}       _    Initial parameters (unused).
 */
export function scroll_position_restorer( node, _ ) {
	const el = node instanceof HTMLElement ? node : document.documentElement;
	/** @type {Collection} */
	let current;
	/** @type {import('svelte/store').Writable<Collection>} */
	const collection = writable( {} );

	collection.subscribe( $value => ( current = $value ) );

	/** @param {string} id */
	function store( id ) {
		collection.update( $value => ( {
			...$value,
			[ id ]: el.scrollTop,
		} ) );
	}

	/** @param {string} id */
	function restore( id ) {
		el.scrollTo( 0, current[ id ] || 0 );
	}

	return {
		/** @param {Param} next_param */
		update( next_param ) {
			if ( ! next_param ) {
				return;
			}

			if ( next_param.restore ) {
				restore( next_param.id );
			} else {
				store( next_param.id );
			}
		},
	};
}
