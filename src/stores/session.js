/**
 * @typedef {import('$types').Auth} Auth
 * @typedef {import('$types').Session} Session
 */

import { browser } from '$app/environment';
import { writable } from 'svelte/store';
import { get_engine } from '$engines';

/**
 * Create session store
 *
 * @param {typeof fetch}                   fetch    SvelteKit's fetch function.
 * @param {import('$types').SettingsStore} settings Settings store.
 */
export function session( fetch, settings ) {
	/** @type {Session} */
	const initial = {
		is_checked: false,
		is_valid: false,
		redirect_to: '',
	};

	/** @type {import('svelte/store').Writable<Session>} */
	const { set, update, ...store } = writable( initial );

	/** @type {import('@macfja/svelte-persistent-store').PersistentStore<Auth>} */
	let auth;
	/** @type {Auth} */
	let $auth;
	/** @type {Session} */
	let $store;
	/** @type {import('$types').Engine} */
	let engine;

	/**
	 * Initialize auth store
	 */
	async function init_auth() {
		if ( ! browser ) {
			throw new Error( 'Auth store can only be created on browser.' );
		}

		const { create_auth_store } = await import( './auth' );
		auth = create_auth_store();
		auth.subscribe( value => ( $auth = value ) );
	}

	/**
	 * Clear session ID from auth store
	 */
	function clear_session_id() {
		auth.update( value => ( { ...value, id: '' } ) );
	}

	/**
	 * Set engine
	 *
	 * @param {import('$types').EngineName} name Engine name.
	 */
	async function set_engine( name ) {
		const engine_func = await get_engine( name );
		engine = engine_func( fetch, auth, settings );
	}

	/**
	 * Save check result
	 *
	 * @param {boolean} is_valid Whether the signin/session check is valid.
	 */
	function save_check_result( is_valid ) {
		update( value => ( {
			...value,
			is_valid,
			is_checked: true,
		} ) );

		if ( ! is_valid ) {
			clear_session_id();
		}
	}

	store.subscribe( value => {
		$store = value;

		if ( $store.is_checked && $store.is_valid ) {
			engine.init();
		}
	} );

	return {
		...store,

		/**
		 * Check session
		 *
		 * @return {Promise<boolean>} Session validity status.
		 */
		async check() {
			if ( $store.is_checked ) {
				return $store.is_valid;
			}

			await init_auth();

			const { engine: engine_name, id, url } = $auth;
			let is_valid = false;

			if ( ! ( id && url ) ) {
				save_check_result( is_valid );
				return is_valid;
			}

			await set_engine( engine_name );

			try {
				is_valid = await engine.validate_session( $auth );
			} catch ( err ) {
				save_check_result( false );
				throw err;
			}

			save_check_result( is_valid );
			return is_valid;
		},

		/**
		 * Reset session to its initial value
		 */
		reset() {
			set( initial );
		},

		/**
		 * Sign in to engine
		 *
		 * @param {import('$types').SigninParams} params Sign in parameters.
		 */
		async signin( params ) {
			await set_engine( params.engine );

			const result = await engine.signin( params );

			auth.update( () => result );

			update( value => ( {
				...value,
				is_valid: true,
			} ) );
		},

		/**
		 * Sign out of current engine's session
		 */
		async signout() {
			await engine.signout( $auth );

			clear_session_id();
			set( {
				...initial,
				is_checked: true,
			} );
		},

		/**
		 * Get auth URL
		 *
		 * @return {string} Url to authenticate to.
		 */
		get_auth_url() {
			if ( ! browser ) {
				return '';
			}

			return $auth?.url || window.location.origin;
		},

		/**
		 * Set redirect path
		 *
		 * @param {string} path Path.
		 */
		set_redirect( path ) {
			update( value => ( {
				...value,
				redirect_to: path,
			} ) );
		},

		get_engine() {
			return engine;
		},
	};
}
