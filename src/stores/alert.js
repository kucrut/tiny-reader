/**
 * @typedef {{
 *   button: string;
 *   cancel_button?: string;
 *   message: string;
 *   title: string;
 *   on_confirm?: () => void;
 * }} AlertStoreValue
 */

import { writable } from 'svelte/store';

export function alert() {
	/** @type {import('svelte/store').Writable<AlertStoreValue|undefined>} */
	const store = writable();

	/** @type {AlertStoreValue|undefined} */
	let current;

	store.subscribe( value => ( current = value ) );

	return {
		...store,

		confirm() {
			if ( current?.on_confirm && typeof current.on_confirm === 'function' ) {
				current.on_confirm();
			}
		},

		reset() {
			store.set( undefined );
		},
	};
}
