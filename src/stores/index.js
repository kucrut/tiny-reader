/** @typedef {ReturnType<init_stores>} Stores */

import { alert } from './alert';
import { notifications } from './notifications';
import { session } from './session';
import { settings } from './settings';
import { ui } from './ui';

/**
 * Initialise all stores
 *
 * @param {typeof fetch} fetch SvelteKit's fetch function.
 */
export function init_stores( fetch ) {
	const settings_store = settings();

	return {
		alert: alert(),
		notifications: notifications(),
		session: session( fetch, settings_store ),
		settings: settings_store,
		ui: ui(),
	};
}
