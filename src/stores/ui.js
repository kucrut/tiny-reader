/**
 * @typedef {{
 *   current_path?: string;
 *   show_sidebar: boolean;
 * }} Value
 */

import { writable } from 'svelte/store';

export function ui() {
	/** @type {import('svelte/store').Writable<Value>} */
	const store = writable( {
		show_sidebar: false,
	} );

	/** @type {boolean} */
	let is_sidebar_open;

	store.subscribe( ( { show_sidebar } ) => ( is_sidebar_open = show_sidebar ) );

	return {
		...store,

		hide_sidebar() {
			store.update( value => ( { ...value, show_sidebar: false } ) );
		},

		toggle_sidebar() {
			store.update( value => ( { ...value, show_sidebar: ! is_sidebar_open } ) );
		},
	};
}
