import { createLocalStorage, persist } from '@macfja/svelte-persistent-store';
import { writable } from 'svelte/store';

export function create_auth_store() {
	/** @type { import('$types').EngineName } */
	const default_engine = 'tt-rss';

	const store = persist(
		writable( {
			engine: default_engine,
			id: '',
			url: '',
		} ),
		createLocalStorage(),
		'tiny-reader-auth',
	);

	return store;
}
