import { createLocalStorage, persist } from '@macfja/svelte-persistent-store';
import { writable } from 'svelte/store';

export function settings() {
	/** @type {import('@macfja/svelte-persistent-store').PersistentStore<import('$types').Settings>} */
	const store = persist(
		writable( {
			fetch_limit: 9,
			fetch_older_first: false,
			fetch_only_unread: false,
			theme: 'system',
			use_low_profile: false,
		} ),
		createLocalStorage(),
		'tiny-reader-settings',
	);

	return {
		...store,

		toggle_fetch_sort() {
			store.update( ( { fetch_older_first, ...rest } ) => ( {
				...rest,
				fetch_older_first: ! fetch_older_first,
			} ) );
		},
	};
}
