/**
 * @typedef {{
 *   icon?: string;
 *   id: string;
 *   text: string;
 *   timeout?: number;
 * }} Item
 * @typedef {Item[]} Value
 */

import { writable } from 'svelte/store';

/** @type {Partial<Item>} */
export const item_defaults = {
	timeout: 1500,
};

export function notifications() {
	/** @type {import('svelte/store').Writable<Value>} */
	const store = writable( [] );

	return {
		...store,

		/**
		 * Add notification
		 *
		 * @param {Item} item Notification item to add.
		 */
		add( item ) {
			store.update( items => [
				...items,
				{
					...item_defaults,
					...item,
				},
			] );
		},

		/**
		 * Remove notification
		 *
		 * @param {Item['id']} id ID of notification item to remove.
		 */
		remove( id ) {
			store.update( items => items.filter( item => item.id !== id ) );
		},
	};
}
