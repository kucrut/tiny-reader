/* eslint-disable space-in-parens */

interface Window {
	__setTheme: ( theme: import('$types').Theme ) => void;
}

declare namespace App {
	interface PageData {
		stores: import('./stores').Stores;
	}
}
