/**
 * Get engine
 *
 * TODO.
 *
 * @param {import('$types').EngineName} name Engine name.
 * @return {Promise<import('$types').EngineFunc>} Engine function.
 */
export async function get_engine( name ) {
	if ( name === 'tt-rss' ) {
		const { tt_rss } = await import( '$engines/tt-rss' );
		return tt_rss;
	}

	throw new Error( 'Engine not found.' );
}
