/** @typedef {import('./types').Category} Category */
/** @typedef {import('./types').CountersStore} CountersStore */
/** @typedef {import('./types').Feed} RawFeed */
/** @typedef {import('./types').SpecialFeed} RawSpecialFeed */
/** @typedef {import('$types').Feed} Feed */
/** @typedef {import('$types').SourceTree} SourceTree */
/** @typedef {import('$types').SourceTreeItem} SourceTreeItem */

import { create_route_path } from '$utils/routes';
import { decode_entities } from '$utils/entities';
import { writable } from 'svelte/store';

/**
 * Get feed icon
 *
 * @param {number} feed_id Feed ID.
 * @return {string} Feed icon.
 */
function get_feed_icon( feed_id ) {
	switch ( feed_id ) {
		case -6:
			return 'checkbox-outline';
		case -4:
			return 'newspaper-outline';
		case -3:
			return 'leaf-outline';
		case -2:
			return 'cloud-upload-outline';
		case -1:
			return 'star-outline';
		case 0:
			return 'file-tray-stacked-outline';
		default:
			return 'rss-outline';
	}
}

/**
 * Normalize feed
 *
 * @param {RawFeed|RawSpecialFeed} feed     TTRSS feed object.
 * @param {CountersStore}          counters Counters store.
 * @return {Feed} Normalized feed.
 */
export function normalize_feed( feed, counters ) {
	const { bare_id, icon, name, unread } = feed;

	return {
		favicon: typeof icon === 'string' && icon.startsWith( 'http' ) ? icon : undefined,
		href: create_route_path( `feed/${ bare_id }` ),
		icon: get_feed_icon( bare_id ),
		id: bare_id,
		is_category_feed: false,
		title: decode_entities( name ),
		unread: counters.get_single( `feed__${ bare_id }` )?.counter || unread,
	};
}

/**
 * Create all articles feed
 *
 * @param {Category}      category Category object.
 * @param {CountersStore} counters Counters store.
 * @return {Feed} Feed.
 */
function create_all_articles_feed( category, counters ) {
	const { bare_id, name } = category;

	return {
		href: create_route_path( `cat/${ bare_id }` ),
		icon: 'newspaper-outline',
		id: bare_id,
		is_category_feed: true,
		title: decode_entities( name ),
		menu_title: 'All Articles',
		unread: counters.get_single( `category__${ bare_id }` )?.counter || 0,
	};
}

/**
 * Normalize source tree item
 *
 * @param {import('./types').Category} category TTRSS category object.
 * @param {CountersStore}              counters Counters store.
 * @return {SourceTreeItem} Normalized source tree item.
 */
function normalize_source_tree_item( category, counters ) {
	const { bare_id, items, name } = category;

	const feeds = items.map( feed => normalize_feed( feed, counters ) );

	return {
		href: create_route_path( `cat/${ bare_id }` ),
		icon: 'newspaper-outline',
		id: bare_id,
		title: decode_entities( name ),
		unread: counters.get_single( `category__${ bare_id }` )?.counter || 0,
		feeds: bare_id >= 0 ? [ create_all_articles_feed( category, counters ), ...feeds ] : feeds,
	};
}

/**
 * Update source tree unread counters
 *
 * @param {SourceTreeItem} item     Source tree item.
 * @param {CountersStore}  counters Counters store.
 * @return {SourceTreeItem} Updated source tree item.
 */
function update_source_tree_counters( item, counters ) {
	return {
		...item,
		unread: counters.get_single( `category__${ item.id }` )?.counter || 0,
		feeds: item.feeds.map( feed => ( {
			...feed,
			unread: feed.is_category_feed ? item.unread : counters.get_single( `feed__${ feed.id }` )?.counter || 0,
		} ) ),
	};
}

/**
 * Create surce tree store
 *
 * @param {import('./request').SessionFetch} fetcher  Fetcher function.
 * @param {CountersStore}                    counters Counters store.
 * @return {import('$types').SourceTreeStore} Source tree store.
 */
export function create_source_tree_store( fetcher, counters ) {
	/** @type {SourceTree} */
	const initial = {
		has_items: false,
		has_more: false,
		is_fetched: false,
		is_fetching: false,
		is_fetching_more: false,
		items: [],
		items_by_id: new Map(),
	};

	/** @type {SourceTree} */
	let $store;
	const { update, ...store } = writable( initial );

	store.subscribe( value => ( $store = value ) );

	counters.subscribe( value => {
		if ( $store.is_fetching || ! value.has_items ) {
			return;
		}

		update( ( { items, ...rest } ) => {
			const new_items = items.map( item => update_source_tree_counters( item, counters ) );

			return {
				...rest,
				items: new_items,
				items_by_id: new Map( new_items.map( item => [ item.id, item ] ) ),
			};
		} );
	} );

	return {
		...store,

		async fetch() {
			update( value => ( {
				...value,
				is_fetching: true,
			} ) );

			await counters.fetch();

			const response = await fetcher( {
				include_empty: true,
				op: 'getFeedTree',
			} );

			if ( ! response.ok ) {
				throw new Error( response.statusText );
			}

			/** @type { import('./types').GetFeedTreeResponse } */
			const data = await response.json();

			if ( data.status === 1 ) {
				throw new Error( data.content.error );
			}

			/** @type {SourceTreeItem[]} */
			const items = [];
			/** @type {[number,SourceTreeItem][]} */
			const items_by_id = [];

			for ( const cat of data.content.categories.items ) {
				const item = normalize_source_tree_item( cat, counters );

				items.push( item );
				items_by_id.push( [ item.id, item ] );
			}

			update( value => ( {
				...value,
				items,
				has_items: items.length > 0,
				is_fetched: true,
				is_fetching: false,
				items_by_id: new Map( items_by_id ),
			} ) );
		},

		get_single( id ) {
			return $store.items_by_id.get( id );
		},
	};
}
