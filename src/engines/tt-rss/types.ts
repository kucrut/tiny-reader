import type { BetterOmit, EngineData } from '$types';
import type { Readable } from 'svelte/store';

export interface Attachment {
	content_type: string;
	content_url: string;
	duration: string;
	height: number;
	id: number;
	post_id: number;
	title: string;
	width: number;
}

export interface Article {
	always_display_attachments: boolean;
	attachments: Attachment[];
	author: string;
	comments_count: number;
	comments_link: string;
	content: string;
	excerpt: string;
	feed_id: number;
	feed_title: string;
	flavor_image: string;
	flavor_stream: string;
	guid: string;
	id: number;
	is_updated: boolean;
	labels: string[];
	lang: string;
	link: string;
	marked: boolean;
	note: string | null;
	published: boolean;
	score: number;
	tags: string[];
	title: string;
	unread: boolean;
	updated: number;
}

export interface UpdateableArticleProperties {
	marked: 0;
	unread: 2;
}

export interface BaseFeed {
	auxcounter: number;
	bare_id: number;
	error: string;
	icon: string;
	id: string;
	name: string;
	unread: number;
}

export interface SpecialFeed extends BaseFeed {
	type: 'feed';
	updated: string;
}

export interface Feed extends BaseFeed {
	checkbox: boolean;
	params: string;
	updates_disabled: 0 | 1;
}

export type Category = {
	bare_id: number;
	id: string;
	items: Feed[];
	name: string;
	type: string;
	unread: number;
};

export interface BaseCounter {
	counter: number;
}

export interface GlobalCounter extends BaseCounter {
	id: 'global-unread';
}

export interface SubscribedFeedsCounter extends BaseCounter {
	id: 'subscribed-feeds';
}

export interface SpecialCounter extends BaseCounter {
	auxcounter?: number;
	id: number;
	markedcounter?: number;
}

export interface CategoryCounter extends SpecialCounter {
	kind?: 'cat';
}

export interface FeedCounter extends SpecialCounter {
	updated: '14:09';
	has_img: number;
	error: string;
	title: string;
}

export type CounterTypes =
	| CategoryCounter
	| Counter
	| FeedCounter
	| GlobalCounter
	| SpecialCounter
	| SubscribedFeedsCounter;

export interface Counter {
	counter: number;
	id: string;
}

interface CountersData< T > extends BetterOmit< EngineData< T >, 'items_by_id' > {
	items_by_id: Map< string, T >;
}

export type Counters = CountersData< Counter >;

export interface CountersStore extends Readable< Counters > {
	fetch: () => Promise< void >;
	get_single: ( id: string ) => Counter | undefined;
}

interface ApiResponseError {
	error: string;
	method?: string;
}

type APIResponseStatus = 1 | 0;

type ApiResponse< S extends APIResponseStatus, C > = {
	content: C;
	seq: number;
	status: S;
};

export type MakeApiResponse< S extends APIResponseStatus, T > = S extends 0
	? ApiResponse< S, T >
	: ApiResponse< S, ApiResponseError >;

export type GetArticleResponse = MakeApiResponse< APIResponseStatus, Article[] >;

export type GetCountersResponse = MakeApiResponse< APIResponseStatus, CounterTypes[] >;

export type GetFeedTreeResponse = MakeApiResponse<
	APIResponseStatus,
	{
		categories: {
			identifier: string;
			items: Category[];
			label: string;
		};
	}
>;

export type SignInResponse = MakeApiResponse<
	APIResponseStatus,
	{
		api_level: number;
		session_id: string;
		config: {
			icons_dir: string;
			icons_url: string;
			daemon_is_running: boolean;
			custom_sort_types: string[];
			num_feeds: number;
		};
	}
>;

export type ValidateSessionResponse = MakeApiResponse<
	APIResponseStatus,
	{
		status: boolean;
	}
>;

export type SubscribeToFeedResponse = MakeApiResponse<
	APIResponseStatus,
	{
		status: {
			code: number;
			feeds?: Record< string, string >;
			message?: string;
		};
	}
>;

export type UnsubscribeFeedResponse = MakeApiResponse<
	APIResponseStatus,
	{
		message: string;
	}
>;
