/** @typedef {import('./request').Fetch} Fetch */
/** @typedef {import('$types').Auth} Auth */

/**
 * Better error message for auth requests
 *
 * @param {string} error Original error message from TTRSS API.
 * @return {string} Improved error message.
 */
function better_error_message( error ) {
	/** @type {string} */
	let message;

	switch ( error ) {
		case 'API_DISABLED':
			message = 'API access is disabled. Please enable it from Tiny Tiny RSS Preferences.';
			break;
		case 'NOT_LOGGED_IN':
			message = 'Your session has expired, please sign in again.';
			break;

		default:
			message = error;
	}

	return message;
}

/**
 * Sign in
 *
 * @param {Fetch}                         fetch  API fetcher.
 * @param {import('$types').SigninParams} params Sign in parameters.
 * @return {Promise<Auth>} Auth store value.
 */
export async function signin( fetch, params ) {
	const { password, url, username } = params;

	const response = await fetch( url, {
		password,
		op: 'login',
		user: username,
	} );

	if ( ! response.ok ) {
		throw new Error( response.statusText );
	}

	/** @type { import('./types').SignInResponse } */
	const data = await response.json();

	if ( data.status === 0 ) {
		return {
			url,
			engine: 'tt-rss',
			id: data.content.session_id,
		};
	}

	throw new Error( better_error_message( data.content.error ) );
}

/**
 * Sign out
 *
 * @param {Fetch} fetch  API fetcher.
 * @param {Auth}  params Auth store value.
 * @return {Promise<boolean>} Sign out result.
 */
export async function signout( fetch, params ) {
	const { id, url } = params;

	await fetch( url, {
		op: 'logout',
		sid: id,
	} );

	// TODO: Should we care if the logout request fails?
	return true;
}

/**
 * Validate auth
 *
 * @param {Fetch} fetch  API fetcher.
 * @param {Auth}  params Auth store value.
 * @return {Promise<boolean>} Validation result.
 */
export async function validate_session( fetch, params ) {
	const { id, url } = params;

	const response = await fetch( url, {
		op: 'isLoggedIn',
		sid: id,
	} );

	if ( ! response.ok ) {
		throw new Error( response.statusText );
	}

	/** @type { import('./types').ValidateSessionResponse } */
	const data = await response.json();

	if ( data.status === 0 ) {
		return data.content?.status === true;
	}

	throw new Error( better_error_message( data.content.error ) );
}
