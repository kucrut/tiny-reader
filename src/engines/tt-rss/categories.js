/** @typedef {import('$types').Categories} Categories */

import { derived } from 'svelte/store';

/**
 * Create categories store
 *
 * @param {import('$types').SourceTreeStore} source_tree Source tree store.
 * @return {import('$types').CategoriesStore} Categories store.
 */
export function create_categories_store( source_tree ) {
	/** @type {Categories} */
	const initial = {
		has_items: false,
		has_more: false,
		is_fetched: false,
		is_fetching: false,
		is_fetching_more: false,
		items: [],
		items_by_id: new Map(),
	};

	const store = derived(
		source_tree,
		$source_tree => {
			const items = $source_tree.items.filter( cat => cat.id >= 0 );

			return {
				...$source_tree,
				items,
				has_items: items.length > 0,
				items_by_id: new Map( items.map( f => [ f.id, f ] ) ),
			};
		},
		initial,
	);

	/** @type {Categories} */
	let $store;

	store.subscribe( value => ( $store = value ) );

	return {
		...store,

		async fetch() {
			// Nothing todo?
		},

		get_single( id ) {
			return $store.items_by_id.get( id );
		},
	};
}
