/** @typedef {import('$types').Feeds} Feeds */

import { derived } from 'svelte/store';

/**
 * Create feeds store
 *
 * @param {import('$types').SourceTreeStore} source_tree Source tree store.
 * @return {import('$types').FeedsStore} Data store.
 */
export function create_feeds_store( source_tree ) {
	/** @type {Feeds} */
	const initial = {
		has_items: false,
		has_more: false,
		is_fetched: false,
		is_fetching: false,
		is_fetching_more: false,
		items: [],
		items_by_id: new Map(),
	};

	const store = derived(
		source_tree,
		$source_tree => {
			/** @type {import('$types').Feed[]} */
			let items = [];

			for ( const cat of $source_tree.items ) {
				items = items.concat( cat.feeds.filter( i => ! i.is_category_feed ) );
			}

			return {
				...$source_tree,
				items,
				has_items: items.length > 0,
				items_by_id: new Map( items.map( f => [ f.id, f ] ) ),
			};
		},
		initial,
	);

	/** @type {Feeds} */
	let $store;

	store.subscribe( value => ( $store = value ) );

	return {
		...store,

		async fetch() {
			// Nothing todo?
		},

		get_single( id ) {
			return $store.items_by_id.get( id );
		},
	};
}
