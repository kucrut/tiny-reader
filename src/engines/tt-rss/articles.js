/** @typedef {import('$types').Article} Article */
/** @typedef {import('$types').Articles} Articles */
/** @typedef {import('./types').UpdateableArticleProperties} UpdateableArticleProperties */
/** @typedef {{feed_id?: number; is_cat?: boolean;}} Params */

import { decode_entities } from '$utils/entities';
import { format_days_ago } from '$utils/date';
import { writable } from 'svelte/store';

/**
 * Normalize attachment
 *
 * @param {import('./types').Attachment} attachment Raw attachment.
 * @return {import('$types').Attachment} Normalized attachment.
 */
function normalize_attachment( attachment ) {
	/** @type {import('$types').Attachment} */
	const result = {
		mime_type: attachment.content_type,
		title: decode_entities( attachment.title ),
		url: attachment.content_url,
	};

	if ( 'duration' in attachment ) {
		result.duration = Number( attachment.duration );
	}

	if ( 'height' in attachment ) {
		result.height = Number( attachment.height );
	}

	if ( 'width' in attachment ) {
		result.width = Number( attachment.width );
	}

	return result;
}

/**
 * Normalize article
 *
 * @param {import('./types').Article} article TTRSS article object.
 * @return {Article} Normalized article object.
 */
function normalize_article( article ) {
	const date = new Date( article.updated * 1000 );

	return {
		attachments: article.attachments ? article.attachments.map( normalize_attachment ) : undefined,
		author: decode_entities( article.author ),
		content: article.content,
		datedisplay: format_days_ago( date ),
		datetime: date.toISOString(),
		excerpt: article.excerpt,
		feed_id: article.feed_id,
		id: article.id,
		link: article.link,
		starred: article.marked,
		thumbnail: article.flavor_image,
		title: decode_entities( article.title ),
		unread: article.unread,
	};
}

/**
 * Create articles store
 *
 * @param {import('./request').SessionFetch} fetcher  Fetcher function.
 * @param {import('./types').CountersStore}  counters Counters store.
 * @param {import('$types').SettingsStore}   settings Settings store.
 * @return {import('$types').ArticlesStore} Articles store.
 */
export function create_articles_store( fetcher, counters, settings ) {
	/** @type {UpdateableArticleProperties} */
	const updateable_properties = {
		marked: 0,
		unread: 2,
	};

	/** @type {import('$types').Settings} */
	let $settings;

	settings.subscribe( value => ( $settings = value ) );

	const fixed_params = {
		include_attachments: true,
		op: 'getHeadlines',
		show_content: true,
		show_excerpt: true,
	};

	/** @type {Params} */
	let $params;

	/** @type {import('svelte/store').Writable<Params>} */
	const params = writable( {} );

	/** @type {Articles} */
	const initial = {
		has_items: false,
		has_more: false,
		is_fetched: false,
		is_fetching: false,
		is_fetching_more: false,
		items: [],
		items_by_id: new Map(),
	};

	/** @type {Articles} */
	let $store;
	const store = writable( initial );

	store.subscribe( value => ( $store = value ) );

	/**
	 * Set error
	 *
	 * @param {string} message Error message.
	 */
	function set_error( message ) {
		store.update( value => ( {
			...value,
			error: new Error( message ),
			is_fetched: true,
			is_fetching: false,
			is_fetching_more: false,
		} ) );
	}

	/**
	 * Fetch articles
	 *
	 * @param {boolean=} more Fetch more?
	 */
	async function fetch( more = false ) {
		store.update( value => ( {
			...value,
			error: null,
			is_fetching: true,
			is_fetching_more: more,
		} ) );

		try {
			let skip = 0;

			if ( more ) {
				if ( $settings.fetch_only_unread ) {
					skip = $store.items.filter( i => i.unread ).length;
				} else {
					skip = $store.items.length;
				}
			}

			const response = await fetcher( {
				...$params,
				...fixed_params,
				skip,
				limit: $settings.fetch_limit,
				order_by: $settings.fetch_older_first ? 'date_reverse' : 'feed_dates',
				view_mode: $settings.fetch_only_unread ? 'unread' : 'all_articles',
			} );

			if ( ! response.ok ) {
				throw new Error( response.statusText );
			}

			/** @type { import('./types').GetArticleResponse } */
			const data = await response.json();

			if ( data.status === 1 ) {
				throw new Error( data.content.error );
			}

			/** @type {[number,Article][]} */
			const new_items_by_id = [];
			/** @type {Article[]} */
			const new_items = [];

			for ( const article of data.content ) {
				const item = normalize_article( article );

				new_items_by_id.push( [ item.id, item ] );
				new_items.push( item );
			}

			const items = more ? [ ...$store.items, ...new_items ] : new_items;
			const items_by_id = more ? [ ...$store.items_by_id, ...new_items_by_id ] : new_items_by_id;

			store.update( value => ( {
				...value,
				items,
				has_items: items.length > 0,
				has_more: $settings.fetch_limit === new_items.length,
				is_fetched: true,
				is_fetching: false,
				is_fetching_more: false,
				items_by_id: new Map( items_by_id ),
			} ) );
		} catch ( error ) {
			const message = error instanceof Error ? error.message : 'Unknown error.';
			set_error( message );
		}
	}

	/**
	 * Update articles
	 *
	 * @param {number[]}                          ids      Article IDs.
	 * @param {keyof UpdateableArticleProperties} property Property to update.
	 * @param {0|1}                               mode     New value.
	 */
	async function update_articles( ids, property, mode ) {
		const key = property === 'marked' ? 'starred' : property;

		try {
			const response = await fetcher( {
				mode,
				article_ids: ids.join( ',' ),
				field: updateable_properties[ property ],
				op: 'updateArticle',
			} );

			if ( ! response.ok ) {
				throw new Error( response.statusText );
			}

			const items = $store.items.map( article => ( {
				...article,
				[ key ]: ids.includes( article.id ) ? Boolean( mode ) : article[ key ],
			} ) );

			store.update( value => ( {
				...value,
				items,
				items_by_id: new Map( items.map( i => [ i.id, i ] ) ),
			} ) );

			counters.fetch();
		} catch ( error ) {
			const message = error instanceof Error ? error.message : 'Unknown error.';
			set_error( message );
		}
	}

	params.subscribe( value => {
		if ( $params?.feed_id !== value.feed_id || $params?.is_cat !== value.is_cat ) {
			store.update( () => initial );
		}

		$params = value;

		if ( typeof value.feed_id === 'number' ) {
			fetch();
		}
	} );

	return {
		...store,

		fetch() {
			return fetch();
		},

		fetch_more() {
			return fetch( true );
		},

		async fetch_single( id ) {
			store.update( value => ( {
				...value,
				error: null,
				is_fetched: false,
				is_fetching: true,
			} ) );

			try {
				const response = await fetcher( {
					article_id: id,
					op: 'getArticle',
				} );

				if ( ! response.ok ) {
					throw new Error( response.statusText );
				}

				/** @type { import('./types').GetArticleResponse } */
				const data = await response.json();

				if ( data.status === 1 || ! data.content.length ) {
					throw new Error( 'Article not found.' );
				}

				const article = normalize_article( data.content[ 0 ] );

				store.update( value => ( {
					...value,
					is_fetched: true,
					is_fetching: false,
					items: value.items.concat( article ),
					items_by_id: new Map( [ ...value.items_by_id, [ id, article ] ] ),
				} ) );
			} catch ( error ) {
				const message = error instanceof Error ? error.message : 'Unknown error.';
				set_error( message );
			}
		},

		async refresh() {
			store.update( () => initial );
			fetch();
		},

		get_params() {
			return $params;
		},

		set_params( new_params ) {
			params.update( value => ( {
				...value,
				...new_params,
			} ) );
		},

		get_single( id ) {
			return $store.items_by_id.get( id );
		},

		get_previous( id ) {
			const keys = Array.from( $store.items_by_id.keys() );
			const prev_index = keys.indexOf( id ) - 1;

			return prev_index >= 0 ? $store.items_by_id.get( keys[ prev_index ] ) : undefined;
		},

		get_next( id ) {
			const keys = Array.from( $store.items_by_id.keys() );
			const next_index = keys.indexOf( id ) + 1;

			return next_index < $store.items.length ? $store.items_by_id.get( keys[ next_index ] ) : undefined;
		},

		toggle_read( ids, is_read ) {
			return update_articles( ids, 'unread', is_read ? 0 : 1 );
		},

		toggle_starred( ids, is_starred ) {
			return update_articles( ids, 'marked', is_starred ? 1 : 0 );
		},
	};
}
