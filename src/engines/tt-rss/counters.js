/** @typedef {import('./types').Counter} Counter */
/** @typedef {import('./types').Counters} Counters */

import { writable } from 'svelte/store';

/**
 * Normalize counter
 *
 * @param {import('./types').CounterTypes} item Counter item to normalize.
 * @return {Counter} Normalized counter.
 */
function normalize_counter( item ) {
	const { counter } = item;

	switch ( item.id ) {
		case 'global-unread':
			return {
				counter,
				id: 'category__-1',
			};

		case 'subscribed-feeds':
			return {
				counter,
				id: item.id,
			};

		default:
			// eslint-disable-next-line no-case-declarations
			const type = 'kind' in item && item.kind === 'cat' ? 'category' : 'feed';

			return {
				counter,
				id: `${ type }__${ item.id }`,
			};
	}
}

/**
 * Create counters store
 *
 * @param {import('./request').SessionFetch} fetcher Fetcher function.
 * @return {import('./types').CountersStore} Counters store.
 */
export function create_counters_store( fetcher ) {
	/** @type {Counters} */
	const initial = {
		has_items: false,
		has_more: false,
		is_fetching: false,
		is_fetched: false,
		is_fetching_more: false,
		items: [],
		items_by_id: new Map(),
	};

	/** @type {Counters} */
	let $store;
	const store = writable( initial );

	store.subscribe( value => ( $store = value ) );

	return {
		...store,

		async fetch() {
			try {
				const response = await fetcher( {
					op: 'getCounters',
					output_mode: 'cf',
				} );

				if ( ! response.ok ) {
					throw new Error( response.statusText );
				}

				/** @type { import('./types').GetCountersResponse } */
				const data = await response.json();
				/** @type {[string,Counter][]} */
				const items_by_id = [];
				/** @type {Counter[]} */
				const items = [];

				if ( data.status === 1 ) {
					throw new Error( data.content.error );
				}

				for ( const counter of data.content ) {
					const item = normalize_counter( counter );

					items_by_id.push( [ item.id, item ] );
					items.push( item );
				}

				store.update( value => ( {
					...value,
					items,
					has_items: items.length > 0,
					is_fetched: true,
					items_by_id: new Map( items_by_id ),
				} ) );
			} catch ( error ) {
				const message = error instanceof Error ? error.message : 'Unknown error.';

				// TODO.
				// eslint-disable-next-line no-console
				console.error( message );
			}
		},

		get_single( id ) {
			return $store.items_by_id.get( id );
		},
	};
}
