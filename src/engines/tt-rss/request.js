/** @typedef {(url:string, data: Record<string,any>) => Promise<Response>} Fetch */
/** @typedef {(data: Record<string,any>) => ReturnType<Fetch>} SessionFetch */

/**
 * Make API fetch function
 *
 * @param {typeof fetch} sveltekit_fetch SvelteKit's fetch.
 * @return {Fetch} Fetch function.
 */
export function make_fetch( sveltekit_fetch ) {
	/**
	 * Request!
	 *
	 * @param {string}             url  TT-RSS URL.
	 * @param {Record<string,any>} data Request data.
	 * @return {Promise<Response>} Fetch response object.
	 */
	function fetch( url, data ) {
		const backend_url = new URL( url );
		const should_proxy = backend_url.host !== window.location.host;

		const final_data = should_proxy ? { ...data, _endpoint: `${ backend_url.href }/api/` } : data;
		const final_url = should_proxy ? '/proxy' : `${ url }/api/`;

		return sveltekit_fetch( final_url, {
			body: JSON.stringify( final_data ),
			method: 'post',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json; charset=utf-8',
			},
		} );
	}

	return fetch;
}

/**
 * Make session fetch
 *
 * @param {Fetch}  fetch Fetch.
 * @param {string} url   API URL.
 * @param {string} sid   Session ID.
 * @return {SessionFetch} SessionFetch function.
 */
export function make_session_fetch( fetch, url, sid ) {
	/**
	 * SessionFetch
	 *
	 * @type {SessionFetch}
	 */
	function session_fetch( data ) {
		return fetch( url, { ...data, sid } );
	}

	return session_fetch;
}
