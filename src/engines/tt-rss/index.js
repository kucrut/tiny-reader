/** @typedef {import('$types').EngineDataTypes} EngineDataTypes */

import { create_articles_store } from './articles';
import { create_categories_store } from './categories';
import { create_counters_store } from './counters';
import { create_feeds_store } from './feeds';
import { create_source_tree_store } from './source-tree';
import { make_fetch, make_session_fetch } from './request';
import { signin, signout, validate_session } from './auth';

/**
 * TTRSS engine
 *
 * @param {typeof fetch}                   sk_fetch SvelteKit's fetch function.
 * @param {import('$types').AuthStore}     auth     Auth store.
 * @param {import('$types').SettingsStore} settings Settings store.
 * @return {import('$types').Engine} Engine object.
 */
export function tt_rss( sk_fetch, auth, settings ) {
	const fetch = make_fetch( sk_fetch );
	let is_initialized = false;

	/** @type { EngineDataTypes['articles'] } */
	let articles;
	/** @type { import('$types').Auth } */
	let current_auth;
	/** @type { import('./types').CountersStore } */
	let counters;
	/** @type { import('./request').SessionFetch } */
	let session_fetch;
	/** @type { EngineDataTypes['source_tree'] } */
	let source_tree;

	auth.subscribe( value => ( current_auth = value ) );

	return {
		signin: ( ...args ) => signin( fetch, ...args ),
		signout: ( ...args ) => signout( fetch, ...args ),
		validate_session: ( ...args ) => validate_session( fetch, ...args ),

		async init() {
			if ( is_initialized ) {
				return;
			}

			const { id, url } = current_auth;

			if ( ! ( id && url ) ) {
				throw new Error( 'TTRSS init: Invalid auth.' );
			}

			session_fetch = make_session_fetch( fetch, url, id );
			counters = create_counters_store( session_fetch );
			articles = create_articles_store( session_fetch, counters, settings );
			source_tree = create_source_tree_store( session_fetch, counters );

			source_tree.fetch();

			is_initialized = true;
		},

		/**
		 * Get data
		 *
		 * @template {keyof EngineDataTypes} K
		 * @param {K} type Data type.
		 * @return {EngineDataTypes[K]} Data store.
		 */
		get_data( type ) {
			return {
				articles,
				source_tree,
				feeds: create_feeds_store( source_tree ),
				categories: create_categories_store( source_tree ),
			}[ type ];
		},

		async subscribe( feed_url, category_id, params = {} ) {
			const response = await session_fetch( {
				...params,
				category_id,
				feed_url,
				op: 'subscribeToFeed',
			} );

			if ( ! response.ok ) {
				return {
					result: response.statusText,
					success: false,
				};
			}

			/** @type { import('./types').SubscribeToFeedResponse } */
			const data = await response.json();

			if ( data.status === 1 ) {
				throw new Error( data.content.error );
			}

			switch ( data.content.status.code ) {
				case 0:
					return {
						message: `Already subscribed to ${ feed_url }.`,
						success: false,
					};

				case 1:
					return {
						message: `Successfully subscribed to ${ feed_url }.`,
						success: true,
					};

				case 2:
					return {
						message: `Could not subscribe to ${ feed_url }.`,
						success: false,
					};

				case 3:
					return {
						message: `No feeds found in ${ feed_url }.`,
						success: false,
					};

				case 4:
					if ( data.content.status.feeds ) {
						return {
							feeds: Object.entries( data.content.status.feeds ).map( ( [ url, title ] ) => ( {
								url,
								title,
							} ) ),
						};
					}

					return {
						message: 'Invalid response from Tiny Tiny RSS.',
						success: false,
					};

				case 5:
					return {
						success: false,
						message:
							data.content.status.message === 'HTTP Code: 401 '
								? 'This feed requires authentication.'
								: data.content.status.message,
					};

				default:
					return {
						message: 'Unknown error.',
						success: false,
					};
			}
		},

		async unsubscribe( feed_id ) {
			if ( typeof feed_id !== 'number' || feed_id < 1 ) {
				return false;
			}

			const response = await session_fetch( {
				feed_id,
				op: 'unsubscribeFeed',
			} );

			if ( ! response.ok ) {
				throw new Error( response.statusText );
			}

			/** @type { import('./types').UnsubscribeFeedResponse } */
			const data = await response.json();

			if ( data.status === 1 ) {
				throw new Error( data.content.error );
			}

			return true;
		},
	};
}
