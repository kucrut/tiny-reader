/* eslint-disable @typescript-eslint/no-var-requires */

module.exports = {
	plugins: [
		require( 'postcss-import' ),
		require( 'postcss-jit-props' )( require( 'open-props' ) ),
		require( '@csstools/postcss-global-data' )( {
			files: [ 'node_modules/open-props/media.min.css', 'src/styles/custom-media.pcss' ],
		} ),
		require( 'postcss-mixins' )( { mixinsDir: 'src/styles/mixins' } ),
		require( 'postcss-nested' ),
		require( 'postcss-url' ),
		require( 'postcss-preset-env' )( {
			stage: 0,
		} ),
		require( 'autoprefixer' ),
	],
};
