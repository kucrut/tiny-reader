/* eslint-disable jsdoc/valid-types */

import a_auto from '@sveltejs/adapter-auto';
import a_node from '@sveltejs/adapter-node';
import a_static from '@sveltejs/adapter-static';
import { vitePreprocess } from '@sveltejs/vite-plugin-svelte';

const { ADAPTER = 'auto', BASE_ROUTE = '' } = process.env;

function adapter() {
	switch ( ADAPTER ) {
		case 'node':
			return a_node( {
				out: 'build/node',
			} );

		case 'static':
			return a_static( {
				fallback: 'index.html',
				pages: `build/spa${ BASE_ROUTE.replace( '/', '-' ) }`,
			} );

		default:
			return a_auto();
	}
}

/**
 * Get base route
 *
 * @return {'' | `/${string}` | undefined } Base route.
 */
function get_base_route() {
	if ( ! BASE_ROUTE ) {
		return '';
	}

	if ( BASE_ROUTE.startsWith( '/' ) ) {
		return /** @type {`/${string}`} */ ( BASE_ROUTE );
	}

	return undefined;
}

/** @type {import('@sveltejs/kit').Config} */
const config = {
	kit: {
		adapter: adapter(),
		alias: {
			$actions: 'src/actions',
			$api: 'src/api',
			$components: 'src/components',
			$engines: 'src/engines',
			$stores: 'src/stores',
			$styles: 'src/styles',
			$types: 'src/types.ts',
			$utils: 'src/utils',
		},
		paths: {
			base: get_base_route(),
		},
		prerender: { entries: [] },
	},
	preprocess: [ vitePreprocess() ],
};

export default config;
