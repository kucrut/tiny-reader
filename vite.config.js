import { parse } from 'node-html-parser';
import { readdirSync, readFileSync, writeFileSync } from 'node:fs';
import { sveltekit } from '@sveltejs/kit/vite';

/** @return {import('vite').Plugin} Vite plugin. */
function svg_sprite() {
	const excluded_attributes = [ 'class', 'height', 'width', 'xmlns' ];
	const source_dir = 'src/icons';
	const target_file = 'src/components/svg-sprite.svelte';

	/**
	 * Translate svg attributes to symbol attributes
	 *
	 * @param {string[]} Attribute pair.
	 * @return {string} Translated attribute pair.
	 */
	const attributes = ( [ key, value ] ) => {
		if ( excluded_attributes.includes( key ) ) {
			return '';
		}

		if ( key === 'stroke-width' ) {
			return 'stroke-width="1"';
		}

		return `${ key }="${ value }"`;
	};

	return {
		name: 'dz-svg-sprite',

		configResolved( config ) {
			const html = readdirSync( source_dir )
				.map( name => {
					const content = readFileSync( `${ source_dir }/${ name }`, { encoding: 'utf-8' } );
					const parsed = parse( content );
					const svg = parsed.querySelector( 'svg' );

					if ( ! svg ) {
						return '';
					}

					const children = svg.removeWhitespace().childNodes.join( '' );

					if ( ! children ) {
						return '';
					}

					const attrs = Object.entries( svg.attributes ).map( attributes ).join( ' ' ).trim();
					const id = name.replace( /\.svg$/, '' );

					return `<symbol id="icon-${ id }" ${ attrs }>${ children }</symbol>`;
				} )
				.join( '' )
				.trim();

			if ( ! html ) {
				return;
			}

			writeFileSync( `${ config.root }/${ target_file }`, `<svg>${ html }</svg>`, { encoding: 'utf-8' } );
		},
	};
}

const config = {
	optimizeDeps: {
		include: [ 'dompurify', 'youtube-player' ],
	},
	plugins: [ sveltekit(), svg_sprite() ],
};

export default config;
