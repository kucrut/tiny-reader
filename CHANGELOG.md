# CHANGELOG

## 1.12.9

### Patch Changes

- 0333d49: Update dependencies
- fc5b04c: Improve SVG sprite generator

## 1.12.8

### Patch Changes

- b32ff32: Fix notifications style
- 814b3b6: Fix mark-as-read when navigating to article page

## 1.12.7

### Patch Changes

- be42bb5: Update dependencies

## 1.12.6

### Patch Changes

- 63326e6: Fix actions bar size & alignment

## 1.12.5

### Patch Changes

- ac29554: Move SVG sprite generator to server hook

## 1.12.4

### Patch Changes

- d448a80: Fix package publishing

## 1.12.3

### Patch Changes

- 65f7e65: Fix packaging

## 1.12.2

### Patch Changes

- 88ad5cd: Fix package publishing

## 1.12.1

### Patch Changes

- dca0c75: Publish Node JS package

## 1.12.0

### Minor Changes

- 469c436: Proxy request to TTRSS backend

## 1.11.6

### Patch Changes

- 0061be9: Bring back scroll position restorer
- 75748a4: Fix no-scroll style
- 5ae71e0: Fix error message styling on single article page

## 1.11.5

### Patch Changes

- fbfde25: Fix article meta alignment

## 1.11.4

### Patch Changes

- c447961: Avoid CLS when loading more content on feed pages

## 1.11.3

### Patch Changes

- 71a8527: Inject critical JS
- c8a7cb9: Process critical css

## 1.11.2

### Patch Changes

- b770684: Fix no-scroll functionality
- 5bbb5f9: Split common styles into utilities & elements

## 1.11.1

### Patch Changes

- 50c4e68: Refactor Layout Style

## 1.11.0

### Minor Changes

- 2bfebb0: Refactor SVG Sprite

## 1.10.4

### Patch Changes

- 7b49f8f: Style fixes after postcss setup clean-up

## 1.10.3

### Patch Changes

- 595ab05: Remove prettier, clean-up postcss setup

## 1.10.2

### Patch Changes

- 8f8d710: Fix "All feeds" counter on update

## 1.10.1

### Patch Changes

- 77e8a7e: Update dependencies (20230708)

## 1.10.0

### Minor Changes

- 9854945: Svelte v4

## 1.9.6

### Patch Changes

- 0db68e8: Update base docker image to nginx:stable-alpine

## 1.9.5

### Patch Changes

- 13d3f01: Make version bump CI pipeline faster
- 1de2928: Fix lint jobs (take 3)
- 684e25e: Clean-up config & stuff™
- 2e7a110: CI: Fix lint jobs

## 1.9.4

### Patch Changes

- 3af0567: SvelteKit 1.0, Node 18

## 1.9.3

### Patch Changes

- a7b021a: Improve formatter
- d8a8b6c: Improve actions on single article

## 1.9.2

### Patch Changes

- f41aa00: Fix attachment check

## 1.9.1

### Patch Changes

- b60141e: Improve attachment handling

## 1.9.0

### Minor Changes

- b0c73a6: Improve attachment & thumbnail display
- 82fff9c: Display image attachments

## 1.8.0

### Minor Changes

- 524fd93: Display article's audio attachments

## 1.7.2

### Patch Changes

- 05db1c4: Move actions to @kucrut/svelte-stuff

## 1.7.1

### Patch Changes

- 2151bc3: Record & restore scroll position on small screens.

## 1.7.0

### Minor Changes

- 3bce846: Improve actions & base handling

### Patch Changes

- 119b241: Provide create_route_path() helper.

## 1.6.2

### Patch Changes

- 271006f: Refactor stores initialisation

## 1.6.1

### Patch Changes

- 46c4185: TTRSS: Fix double update in source tree store

## 1.6.0

### Minor Changes

- 062e0d9: TTRSS: Better error messages for auth requests

### Patch Changes

- e5d3fd8: Fix source tree refresh side effect
- ec6b903: Remove Lazy's default slot

## 1.5.0

### Minor Changes

- a776276: Update TTRSS feed icons

## 1.4.1

### Patch Changes

- ff32043: Remove default border-radius

## 1.4.0

### Minor Changes

- 16ea155: Improve Modal component
- 016652f: Improve Lazy component

### Patch Changes

- 8750113: Improve action style on Article page
- 8a2a696: Strict check when changing theme from settings page

## 1.3.8

### Patch Changes

- 3d1c14b: Improve session check + redirect flow
- 17bfa4e: Improve TTRSS api response typings

## 1.3.7

### Patch Changes

- f6b4183: Fix category feed page

## 1.3.6

### Patch Changes

- bfcf023: Fix sidebar categories list

## 1.3.5

### Patch Changes

- f76741e: Destruct store value as needed

## 1.3.4

### Patch Changes

- 3219254: Fix store usage in ArticlesList

## 1.3.3

### Patch Changes

- 7669f64: Remove custom scroll position handling

## 1.3.2

### Patch Changes

- 794e0e2: Fix TT-RSS source tree fetch: Update is_fetching first thing

## 1.3.1

### Patch Changes

- dc3a6e5: Fix error on first load caused by navigation

## 1.3.0

### Minor Changes

- 47ff8ad: Decouple TTRSS engine
- 73f195b: Refactor scroll position handling

### Patch Changes

- 007bb4e: Add typings
- 4d2acbf: Replace svelte-youtube with svelte-lite-youtube-embed

## 1.2.0

### Minor Changes

- cc8f0ea: Update routes

### Patch Changes

- 8bb3db5: Clean-up babel config

## 1.1.7

### Patch Changes

- b12c964: Improve package archives (pack the directories instead of the content).

## 1.1.6

### Patch Changes

- 054f48b: CI: Fix compose package creator

## 1.1.5

### Patch Changes

- 0df5847: Publish docker compose package

## 1.1.4

### Patch Changes

- 9d90fcb: CI: Improve release name & description
- 9d90fcb: SvelteKit 1.0.0-next.392
- 9d90fcb: Clean-up build script

## 1.1.3

### Patch Changes

- d098aad: CI: Fix releaser & packager

## 1.1.2

### Patch Changes

- 7497b6a: CI: Don't run create_release_mr if CHANGELOG.md changed
- 500e710: CI: Clean-up checked files on MR jobs

## 1.1.1

### Patch Changes

- fdc30ec: Improve release flow
- 801fd3d: Ignore .pnpm-store dir
- aeeb626: CI: Fix release MR creator

## 1.1.0

### Minor Changes

- Upgrade to vite v3

## 20220617

- Dependency refresh

## 20220504

- Dependency refresh

## 20211130

- Fix tooltip style
- Fix feeds list on sidebar

## 20211129

Initial public release
